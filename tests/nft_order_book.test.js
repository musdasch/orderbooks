// Right click on the script name and hit "Run" to execute
const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("ERC721OrderBook", function () {
  let accounts;
  
  let contractGate;
  let feeManager;

  let orderBook;

  let erc20TestToken;
  let erc721TestToken;

  before(async () => {
    return new Promise(async (resolve, reject) => {
      try {
        const ContractGate = await ethers.getContractFactory("ContractGate");
        const FeeManager = await ethers.getContractFactory("FeeManager");
        const ERC721OrderBook = await ethers.getContractFactory("ERC721OrderBook");

        const ERC20TestToken = await ethers.getContractFactory("ERC20TestToken");
        const ERC721TestToken = await ethers.getContractFactory("ERC721TestToken");
      

        accounts = await web3.eth.getAccounts();

        contractGate = await ContractGate.deploy();
        feeManager = await FeeManager.deploy(accounts[0]);
        orderBook = await ERC721OrderBook.deploy(contractGate.address, feeManager.address);

        erc20TestToken = await ERC20TestToken.deploy();
        erc721TestToken = await ERC721TestToken.deploy();

        await orderBook.deployed();
        await feeManager.deployed();
        await contractGate.deployed();
        await erc20TestToken.deployed();
        await erc721TestToken.deployed();

        resolve();
      } catch (e) {
        reject(e);
      }
    });
  });

  it("test initial value", async () => {
    return new Promise(async (resolve, reject) => {
      try {
        expect((await contractGate.owner()).toString()).to.equal(accounts[0]);
        expect((await orderBook.getOrderCount()).toNumber()).to.equal(0);
        
        resolve();
      } catch(e) {
        reject(e);
      }
    });
  });

  /*
  it("test listing with erc20", async () => {
    return new Promise(async (resolve, reject) => {
      await erc721TestToken.safeMint(accounts[0]);

      await erc721TestToken.approve(orderBook.address, 0);
      await orderBook.list(erc721TestToken.address, 0, erc20TestToken.address, 100);

      expect((await orderBook.getOrderCount()).toNumber()).to.equal(1);
      expect((await orderBook.getOrderNFTContract(0)).toString()).to.equal(erc721TestToken.address);
      expect((await orderBook.getOrderTokenId(0)).toNumber()).to.equal(0);
      expect((await orderBook.getOrderOrderMaker(0)).toString()).to.equal(accounts[0]);
      expect((await orderBook.getOrderOrderTaker(0)).toString()).to.equal("0x0000000000000000000000000000000000000000");
      expect((await orderBook.getOrderCurrency(0)).toString()).to.equal(erc20TestToken.address);
      expect((await orderBook.getOrderPrice(0)).toNumber()).to.equal(100);
      expect((await orderBook.getOrderStatus(0)).toString()).to.equal("0");

      resolve()
    });
  });
  */


  /*
  it("test list token with ERC20 and ERC721", async function () {
    const ERC721OrderBook = await ethers.getContractFactory("ERC721OrderBook");
    const ContractGate = await ethers.getContractFactory("ContractGate");

    const ERC20TestToken = await ethers.getContractFactory("ERC20TestToken");
    const ERC721TestToken = await ethers.getContractFactory("ERC721TestToken");

    const erc20TestToken = await ERC20TestToken.deploy();
    const erc721TestToken = await ERC721TestToken.deploy();

    const contractGate = await ContractGate.deploy();

    await contractGate.trust(erc20TestToken.address);
    await contractGate.trust(erc721TestToken.address);

    const orderBook = await ERC721OrderBook.deploy(contractGate.address);
    
    await orderBook.deployed();
    await contractGate.deployed();
    await erc20TestToken.deployed();
    await erc721TestToken.deployed();

    const accounts = await web3.eth.getAccounts();

    await erc20TestToken.mint(accounts[1], 100);
    await erc721TestToken.safeMint(accounts[0]);

    await erc721TestToken.approve(orderBook.address, 0);
    await orderBook.list(erc721TestToken.address, 0, erc20TestToken.address, 100);

    expect((await orderBook.getOrderCount()).toNumber()).to.equal(1);
    expect((await orderBook.getOrderNFTContract(0)).toString()).to.equal(erc721TestToken.address);
    expect((await orderBook.getOrderTokenId(0)).toNumber()).to.equal(0);
    expect((await orderBook.getOrderOrderMaker(0)).toString()).to.equal(accounts[0]);
    expect((await orderBook.getOrderOrderTaker(0)).toString()).to.equal("0x0000000000000000000000000000000000000000");
    expect((await orderBook.getOrderCurrency(0)).toString()).to.equal(erc20TestToken.address);
    expect((await orderBook.getOrderPrice(0)).toNumber()).to.equal(100);
    expect((await orderBook.getOrderStatus(0)).toString()).to.equal("0");
  });
  */
});