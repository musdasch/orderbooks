// SPDX-License-Identifier: WTFPL
pragma solidity ^0.8.3;

interface IERC20OrderBook {

    event Listed(
        uint256 indexed orderId,
        address orderMaker,
        address indexed askCurrency,
        address indexed bidCurrency,
        uint256 askValue,
        uint256 bidValue
    );

    event Unlisted(
        uint256 indexed orderId
    );

    event Fulfilled(
        uint256 indexed orderId,
        address orderMaker,
        address orderTaker,
        address indexed askCurrency,
        address indexed bidCurrency,
        uint256 askValue,
        uint256 bidValue
    );

    enum Status{ OPEN, EXECUTED, CANCELLED }

    struct Order { 
        address payable orderMaker;
        address orderTaker;
        address askCurrency;
        address bidCurrency;
        uint256 askValue;
        uint256 bidValue;
        Status status;
    }

    function list(
        address askCurrency,
        address bidCurrency,
        uint256 askValue,
        uint256 bidValue
    )
    external
    returns (uint256);

    function unlist(uint256 orderId)
    external;
    
    function fulfill(uint256 orderId)
    external payable;

    function getOrderMaker(uint256 orderId)
    external view returns (address);

    function getOrderTaker(uint256 orderId)
    external view returns (address);

    function getAskCurrency(uint256 orderId)
    external view returns (address);

    function getBidCurrency(uint256 orderId)
    external view returns (address);

    function getAskValue(uint256 orderId)
    external view returns (uint256);

    function getBidValue(uint256 orderId)
    external view returns (uint256);

    function getStatus(uint256 orderId)
    external view returns (Status);

    function getOrderCount()
    external view returns (uint256);
}