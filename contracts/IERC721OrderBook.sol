// SPDX-License-Identifier: WTFPL
pragma solidity ^0.8.3;

interface IERC721OrderBook {

    event Listed(
        uint256 indexed orderId,
        address orderMaker,
        address indexed currency,
        address indexed nftContract,
        uint256 price,
        uint256 tokenId
    );

    event Unlisted(
        uint256 indexed orderId
    );

    event Fulfilled(
        uint256 indexed orderId,
        address orderMaker,
        address orderTaker,
        address indexed currency,
        address indexed nftContract,
        uint256 price,
        uint256 tokenId
    );

    enum Status{ OPEN, EXECUTED, CANCELLED }

    struct Order { 
        address payable orderMaker;
        address orderTaker;
        address currency;
        address nftContract;
        uint256 price;
        uint256 tokenId;
        Status status;
    }

    function list(
        address currency,
        address nftContract,
        uint256 price,
        uint256 tokenId
    )
    external
    returns (uint256);

    function unlist(uint256 orderId)
    external;
    
    function fulfill(uint256 orderId)
    external payable;

    function getOrderMaker(uint256 orderId)
    external view returns (address);

    function getOrderTaker(uint256 orderId)
    external view returns (address);

    function getCurrency(uint256 orderId)
    external view returns (address);

    function getNFTContract(uint256 orderId)
    external view returns (address);

    function getPrice(uint256 orderId)
    external view returns (uint256 price);

    function getTokenId(uint256 orderId)
    external view returns (uint256);

    function getStatus(uint256 orderId)
    external view returns (Status);

    function getOrderCount()
    external view returns (uint256);
}