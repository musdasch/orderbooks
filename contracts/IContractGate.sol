// SPDX-License-Identifier: WTFPL
pragma solidity ^0.8.3;

/**
 * Interface of the ContractGate in order to check
 * if a contract is trustworthy against a central list.
 */
interface IContractGate {

    /**
     * returns true if `contract` is trustworthy.
     */
    function isTrusted(address _contract) external view returns(bool);
}