// SPDX-License-Identifier: WTFPL
pragma solidity ^0.8.3;

import "@openzeppelin/contracts@4.6.0/access/Ownable.sol";

import "./IContractGate.sol";

contract ContractGate is IContractGate, Ownable {
    mapping(address => bool) private contracts;

    function isTrusted(address _contract)
    public view virtual override
    returns(bool) {
        return contracts[_contract];
    }

    function trust(address _contract)
    public onlyOwner {
        contracts[_contract] = true;
    }

    function untrust(address _contract)
    public onlyOwner {
        contracts[_contract] = false;
    }
}