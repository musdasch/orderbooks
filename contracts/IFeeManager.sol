// SPDX-License-Identifier: WTFPL
pragma solidity ^0.8.3;

/**
 * Interface of the FeeManager in order to control
 * the fee address on a central point.
 */
interface IFeeManager {

    /**
     * Returns the address who should receive the fee.
     */
    function getFeeAddress() external view returns(address payable);
}
