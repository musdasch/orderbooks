// SPDX-License-Identifier: WTFPL
pragma solidity ^0.8.3;

import "@openzeppelin/contracts@4.6.0/access/Ownable.sol";
import "@openzeppelin/contracts@4.6.0/utils/Counters.sol";
import "@openzeppelin/contracts@4.6.0/utils/math/SafeMath.sol";

import "@openzeppelin/contracts@4.6.0/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts@4.6.0/token/ERC20/IERC20.sol";

import "./IERC721OrderBook.sol";
import "./IContractGate.sol";
import "./IFeeManager.sol";

contract ERC721OrderBook is IERC721OrderBook, Ownable {
    using Counters for Counters.Counter;

    mapping(uint => Order) private _book;
    IContractGate private _contractGate;
    IFeeManager private _feeManager;
    Counters.Counter private _orderIds;

    modifier onlyOpen(uint256 orderId) {
        require(
            Status.OPEN == _book[orderId].status,
            "ERROR: Order is not open."
        );
        _;
    }

    modifier noZeroID(uint256 tokenId ){
        require(
            0 != tokenId,
            "ERROR: This order book can't have a ERC721 token with the ID '0'."
        );
        _;
    }

    modifier onlyOrderOwner(uint256 orderId) {
        require(
            address(msg.sender) == _book[orderId].orderMaker,
            "ERROR: You are not the owner of this order."
        );
        _;
    }

    modifier notOrderOwner(uint256 orderId) {
        require(
            address(msg.sender) != _book[orderId].orderMaker,
            "ERROR: You cannot fulfill your own order."
        );
        _;
    }

    modifier onlyValidOrderIds(uint256 orderId) {
        require(
            0 <= orderId,
            "ERROR: Order ID has to be bigger than zero."
        );
        require(
            _orderIds.current() > orderId,
            "ERROR: Order ID can't be bigger then order book size."
        );
        _;
    }

    modifier onlyApprovedNFT(address nftContract, uint256 tokenId) {
        require(
            IERC721(nftContract).getApproved(tokenId) == address(this),
            "ERROR: Transfer of this NFT has not been approved."
        );
        _;
    }

    modifier onlyTrusted(address _contract) {
        require(
            _contractGate.isTrusted(_contract),
            "ERROR: This contract is not allowed on this order book."
        );
        _;
    }

    modifier minValue(uint256 value) {
        require(
            1000 <= value,
            "ERROR: Minimum price is too low."
        );
        _;
    }

    constructor(address contractGate, address feeManager) {
        _contractGate = IContractGate(contractGate);
        _feeManager = IFeeManager(feeManager);
    }

    function list(
            address currency,
            address nftContract,
            uint256 price,
            uint256 tokenId
        )
            public virtual override
            onlyApprovedNFT(nftContract, tokenId)
            noZeroID(tokenId)
            onlyTrusted(nftContract)
            onlyTrusted(currency)
            minValue(price)
            returns (uint256)
        {

        uint256 newOrderId = _orderIds.current();
        _orderIds.increment();

        _book[newOrderId] = Order(
            payable(msg.sender),
            address(0),
            currency,
            nftContract,
            price,
            tokenId,
            Status.OPEN
        );

        emit Listed(
            newOrderId,
            address(msg.sender),
            currency,
            nftContract,
            price,
            tokenId
        );

        IERC721(nftContract).transferFrom(msg.sender, address(this), tokenId);
        return newOrderId;
    }

    function unlist(uint256 orderId) 
        public virtual override
        onlyOrderOwner(orderId)
        onlyOpen(orderId)
        onlyValidOrderIds(orderId)
        onlyTrusted(_book[orderId].nftContract)
        onlyTrusted(_book[orderId].currency)
    {
        _book[orderId].status = Status.CANCELLED;
        uint256 _tokenId = _book[orderId].tokenId;
        _book[orderId].tokenId = 0;
        _book[orderId].price = 0;

        emit Unlisted(orderId);

        IERC721(_book[orderId].nftContract).transferFrom(
            address(this),
            _book[orderId].orderMaker,
           _tokenId
        );
    }

    function fulfill(uint256 orderId) 
        public payable virtual override
        notOrderOwner(orderId)
        onlyOpen(orderId)
        onlyValidOrderIds(orderId)
        onlyTrusted(_book[orderId].nftContract)
        onlyTrusted(_book[orderId].currency)
    {
        _book[orderId].status = Status.EXECUTED;
        _book[orderId].orderTaker = msg.sender;
        
        if(address(0) != _book[orderId].currency) {
            fulfillERC20(orderId);
        } else {
            fulfillMintMe(orderId);
        }
    }

    function fulfillERC20(uint256 orderId)
    private {
        uint256 _price = _book[orderId].price;
        _book[orderId].price = 0;

        uint256 _tokenId = _book[orderId].tokenId;
        _book[orderId].tokenId = 0;

        uint256 allowance = IERC20(_book[orderId].currency).allowance(
            msg.sender,
            address(this)
        );
        require(
            allowance >= _price,
            "ERROR: Check the token allowance."
        );

        emit Fulfilled(
            orderId,
            _book[orderId].orderMaker,
            address(msg.sender),
            _book[orderId].currency,
            _book[orderId].nftContract,
            _price,
            _tokenId
        );

        uint256 _fee = calcFee(_price);
        _price = SafeMath.sub(_price, _fee);

        IERC20(_book[orderId].currency).transferFrom(
            msg.sender,
            _book[orderId].orderMaker,
            _price
        );

        IERC20(_book[orderId].currency).transferFrom(
            msg.sender,
            _feeManager.getFeeAddress(),
            _fee
        );

        IERC721(_book[orderId].nftContract).transferFrom(
            address(this),
            msg.sender,
            _tokenId
        );
    }

    function fulfillMintMe(uint256 orderId)
    private {
        uint256 _price = _book[orderId].price;
        _book[orderId].price = 0;

        uint256 _tokenId = _book[orderId].tokenId;
        _book[orderId].tokenId = 0;

        require(
            msg.value >= _price,
            "ERROR: The price is higher than the value sent."
        );

        emit Fulfilled(
            orderId,
            _book[orderId].orderMaker,
            address(msg.sender),
            _book[orderId].currency,
            _book[orderId].nftContract,
            _price,
            _tokenId
        );

        uint256 _fee = calcFee(_price);
        _price = SafeMath.sub(_price, _fee);

        (bool success, ) = _book[orderId].orderMaker.call{value: _price}("");
        require(success, "ERROR: MintMe transfer failed");

        (bool successFee, ) = _feeManager.getFeeAddress().call{value: _fee}("");
        require(successFee, "ERROR: MintMe transfer failed in fee");

        IERC721(_book[orderId].nftContract).transferFrom(
            address(this),
            msg.sender,
            _tokenId
        );
    }

    function getOrderMaker(uint256 orderId)
    public view virtual override
    onlyValidOrderIds(orderId)
    returns(address orderMaker) {
        return _book[orderId].orderMaker;
    }

    function getOrderTaker(uint256 orderId)
    public view virtual override
    onlyValidOrderIds(orderId)
    returns(address orderTaker) {
        return _book[orderId].orderTaker;
    }

    function getCurrency(uint256 orderId)
    public view virtual override
    onlyValidOrderIds(orderId)
    returns(address currency) {
        return _book[orderId].currency;
    }

    function getNFTContract(uint256 orderId)
    public view virtual override
    onlyValidOrderIds(orderId)
    returns(address nftContract) {
        return _book[orderId].nftContract;
    }

    function getPrice(uint256 orderId)
    public view virtual override
    onlyValidOrderIds(orderId)
    returns(uint256 price) {
        return _book[orderId].price;
    }

    function getTokenId(uint256 orderId)
    public view virtual override
    onlyValidOrderIds(orderId)
    returns(uint256 tokenId) {
        return _book[orderId].tokenId;
    }

    function getStatus(uint256 orderId)
    public view virtual override
    onlyValidOrderIds(orderId)
    returns(Status status) {
        return _book[orderId].status;
    }

    function getOrderCount()
    public view virtual override
    returns (uint256 count) {
        return _orderIds.current();
    }

    function setContractGate(address contractGate)
    public
    onlyOwner {
        _contractGate = IContractGate(contractGate);
    }

    function setFeeManager(address feeManager)
    public
    onlyOwner {
        _feeManager = IFeeManager(feeManager);
    }

    function calcFee(uint256 price)
    private pure returns (uint256 fee) {
        return SafeMath.div(price, 1000);
    }
}
