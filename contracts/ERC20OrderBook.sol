// SPDX-License-Identifier: WTFPL
pragma solidity ^0.8.3;

import "@openzeppelin/contracts@4.6.0/access/Ownable.sol";
import "@openzeppelin/contracts@4.6.0/utils/Counters.sol";
import "@openzeppelin/contracts@4.6.0/utils/math/SafeMath.sol";

import "@openzeppelin/contracts@4.6.0/token/ERC20/IERC20.sol";

import "./IERC20OrderBook.sol";
import "./IContractGate.sol";
import "./IFeeManager.sol";

contract ERC20OrderBook is IERC20OrderBook, Ownable {
    using Counters for Counters.Counter;

    mapping(uint => Order) private _book;
    IContractGate private _contractGate;
    IFeeManager private _feeManager;
    Counters.Counter private _orderIds;

    modifier onlyOpen(uint256 orderId) {
        require(
            Status.OPEN == _book[orderId].status,
            "ERROR: Order is not open."
        );
        _;
    }

    modifier onlyOrderOwner(uint256 orderId) {
        require(
            address(msg.sender) == _book[orderId].orderMaker,
            "ERROR: You are not the owner of this order."
        );
        _;
    }

    modifier notOrderOwner(uint256 orderId) {
        require(
            address(msg.sender) != _book[orderId].orderMaker,
            "ERROR: You cannot fulfill your own order."
        );
        _;
    }

    modifier onlyValidOrderIds(uint256 orderId) {
        require(
            0 <= orderId,
            "ERROR: Order ID has to be bigger than zero."
        );
        require(
            _orderIds.current() > orderId,
            "ERROR: Order ID can't be bigger then order book size."
        );
        _;
    }

    modifier onlyTrusted(address _contract) {
        require(
            _contractGate.isTrusted(_contract),
            "ERROR: This contract is not allowed on this order book."
        );
        _;
    }

    modifier minValue(uint256 value) {
        require(
            5000 <= value,
            "ERROR: Minimum value is 5000."
        );
        _;
    }

    constructor(address contractGate, address feeManager) {
        _contractGate = IContractGate(contractGate);
        _feeManager = IFeeManager(feeManager);
    }

    function list(
            address askCurrency,
            address bidCurrency,
            uint256 askValue,
            uint256 bidValue
        )
            public virtual override
            onlyTrusted(askCurrency)
            onlyTrusted(bidCurrency)
            minValue(askValue)
            minValue(bidValue)
            returns (uint256)
        {

        uint256 newOrderId = _orderIds.current();
        _orderIds.increment();

        _book[newOrderId] = Order(
            payable(msg.sender),
            address(0),
            askCurrency,
            bidCurrency,
            askValue,
            bidValue,
            Status.OPEN
        );

        uint256 _bidFee = calcFee(bidValue);
        uint256 _bidValue = SafeMath.sub(bidValue, _bidFee);

        emit Listed(
            newOrderId,
            address(msg.sender),
            askCurrency,
            bidCurrency,
            askValue,
            _bidValue
        );

        IERC20(bidCurrency).transferFrom(
            msg.sender,
            address(this),
            bidValue
        );

        return newOrderId;
    }

    function unlist(uint256 orderId) 
        public virtual override
        onlyOrderOwner(orderId)
        onlyOpen(orderId)
        onlyValidOrderIds(orderId)
        onlyTrusted(_book[orderId].askCurrency)
        onlyTrusted(_book[orderId].bidCurrency)
    {
        _book[orderId].status = Status.CANCELLED;
        uint256 _bidValue = _book[orderId].bidValue;
        _book[orderId].askValue = 0;
        _book[orderId].bidValue = 0;

        emit Unlisted(orderId);

        IERC20(_book[orderId].bidCurrency).transferFrom(
            address(this),
            _book[orderId].orderMaker,
            _bidValue
        );
    }

    function fulfill(uint256 orderId) 
        public payable virtual override
        notOrderOwner(orderId)
        onlyOpen(orderId)
        onlyValidOrderIds(orderId)
        onlyTrusted(_book[orderId].askCurrency)
        onlyTrusted(_book[orderId].bidCurrency)
    {
        _book[orderId].status = Status.EXECUTED;
        _book[orderId].orderTaker = msg.sender;
        
        if(address(0) != _book[orderId].askCurrency) {
            fulfillERC20(orderId);
        } else {
            fulfillMintMe(orderId);
        }
    }

    function fulfillERC20(uint256 orderId)
    private {
        uint256 _askValue = _book[orderId].askValue;
        _book[orderId].askValue = 0;

        uint256 _bidValue = _book[orderId].bidValue;
        _book[orderId].bidValue = 0;

        uint256 allowance = IERC20(_book[orderId].askCurrency).allowance(
            msg.sender,
            address(this)
        );
        require(
            allowance >= _askValue,
            "ERROR: Check the token allowance."
        );

        uint256 _askFee = calcFee(_askValue);
        _askValue = SafeMath.sub(_askValue, _askFee);

        emit Fulfilled(
            orderId,
            _book[orderId].orderMaker,
            address(msg.sender),
            _book[orderId].askCurrency,
            _book[orderId].bidCurrency,
            _askValue,
            _bidValue
        );

        uint256 _bidFee = calcFee(_bidValue);
        _bidValue = SafeMath.sub(_bidValue, _bidFee);

        IERC20(_book[orderId].askCurrency).transferFrom(
            msg.sender,
            _book[orderId].orderMaker,
            _askValue
        );

        IERC20(_book[orderId].askCurrency).transferFrom(
            msg.sender,
            _feeManager.getFeeAddress(),
            _askFee
        );

        IERC20(_book[orderId].bidCurrency).transferFrom(
            address(this),
            msg.sender,
            _bidValue
        );

        IERC20(_book[orderId].bidCurrency).transferFrom(
            address(this),
            _feeManager.getFeeAddress(),
            _bidFee
        );
    }

    function fulfillMintMe(uint256 orderId)
    private {
        uint256 _askValue = _book[orderId].askValue;
        _book[orderId].askValue = 0;

        uint256 _bidValue = _book[orderId].bidValue;
        _book[orderId].bidValue = 0;

        require(
            msg.value >= _askValue,
            "ERROR: The price is higher than the value sent."
        );

        uint256 _askFee = calcFee(_askValue);
        _askValue = SafeMath.sub(_askValue, _askFee);

        emit Fulfilled(
            orderId,
            _book[orderId].orderMaker,
            address(msg.sender),
            _book[orderId].askCurrency,
            _book[orderId].bidCurrency,
            _askValue,
            _bidValue
        );

        uint256 _bidFee = calcFee(_bidValue);
        _bidValue = SafeMath.sub(_bidValue, _bidFee);

        (bool success, ) = _book[orderId].orderMaker.call{value: _askValue}("");
        require(success, "ERROR: MintMe transfer failed");

        (bool successFee, ) = _feeManager.getFeeAddress().call{value: _askFee}("");
        require(successFee, "ERROR: MintMe transfer failed in fee");

        IERC20(_book[orderId].bidCurrency).transferFrom(
            address(this),
            msg.sender,
            _bidValue
        );

        IERC20(_book[orderId].bidCurrency).transferFrom(
            address(this),
            _feeManager.getFeeAddress(),
            _bidFee
        );
    }

    function getOrderMaker(uint256 orderId)
    public view virtual override
    onlyValidOrderIds(orderId)
    returns(address orderMaker) {
        return _book[orderId].orderMaker;
    }

    function getOrderTaker(uint256 orderId)
    public view virtual override
    onlyValidOrderIds(orderId)
    returns(address orderTaker) {
        return _book[orderId].orderTaker;
    }

    function getAskCurrency(uint256 orderId)
    public view virtual override
    onlyValidOrderIds(orderId)
    returns(address askCurrency) {
        return _book[orderId].askCurrency;
    }

    function getBidCurrency(uint256 orderId)
    public view virtual override
    onlyValidOrderIds(orderId)
    returns(address BidCurrency) {
        return _book[orderId].bidCurrency;
    }

    function getAskValue(uint256 orderId)
    public view virtual override
    onlyValidOrderIds(orderId)
    returns(uint256 askValue) {
        return _book[orderId].askValue;
    }

    function getBidValue(uint256 orderId)
    public view virtual override
    onlyValidOrderIds(orderId)
    returns(uint256 bidValue) {
        uint256 _bidFee = calcFee(_book[orderId].bidValue);
        uint256 _bidValue = SafeMath.sub(_book[orderId].bidValue, _bidFee);
        return _bidValue;
    }

    function getStatus(uint256 orderId)
    public view virtual override
    onlyValidOrderIds(orderId)
    returns(Status status) {
        return _book[orderId].status;
    }

    function getOrderCount()
    public view virtual override
    returns (uint256 count) {
        return _orderIds.current();
    }

    function setContractGate(address contractGate)
    public
    onlyOwner {
        _contractGate = IContractGate(contractGate);
    }

    function setFeeManager(address feeManager)
    public
    onlyOwner {
        _feeManager = IFeeManager(feeManager);
    }

    function calcFee(uint256 price)
    private pure returns (uint256 fee) {
        return SafeMath.div(price, 5000);
    }
}
