// SPDX-License-Identifier: WTFPL
pragma solidity ^0.8.3;

import "@openzeppelin/contracts@4.6.0/access/Ownable.sol";

import "./IFeeManager.sol";

contract FeeManager is IFeeManager, Ownable {
    address private _feeAddress;

    constructor(address feeAddress) {
        _feeAddress = feeAddress;
    }

    function getFeeAddress()
    public view virtual override
    returns(address payable) {
        return payable(_feeAddress);
    }

    function setFeeAddress(address feeAddress)
    public onlyOwner {
        _feeAddress = feeAddress;
    }
}
